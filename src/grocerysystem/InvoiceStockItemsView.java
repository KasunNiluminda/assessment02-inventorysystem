/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grocerysystem;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.Vector;

/**
 *
 * @author DELL
 */
public class InvoiceStockItemsView extends javax.swing.JFrame {

    private InvoiceView invoiceView;
    Vector vector = new Vector();
    int listIndex = 0;

    /**
     * Creates new form SupplierSearch
     */
    public InvoiceStockItemsView() {
        initComponents();
        setLocationRelativeTo(null);

    }

    InvoiceStockItemsView(InvoiceView aThis, Vector v) {
        this();
        this.invoiceView = aThis;
        invoiceView.txt_unitPrice.setBackground(Color.white);
        invoiceView.txt_qtyAvb.setBackground(Color.white);
        for (int index = 0; index < invoiceView.loopCount * 3;) {
            String id = (String) v.get(index);
            String name = invoiceView.txt_itemName.getText();
            index++;
            String unitPrice = (String) v.get(index);
            index++;
            String qtyAvb = (String) v.get(index);
            ++index;
            String listRow = id + " - " + name + " - " + unitPrice + " - " + qtyAvb;
            vector.add(listRow);
        }
        list_stockItems.setListData(vector);
        list_stockItems.setSelectedIndex(listIndex);
        stockGetails();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        list_stockItems = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        list_stockItems.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        list_stockItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                list_stockItemsMouseClicked(evt);
            }
        });
        list_stockItems.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                list_stockItemsKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(list_stockItems);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("ID - Item Name - Unit Price - QtyAvb");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(50, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void list_stockItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_list_stockItemsMouseClicked

    }//GEN-LAST:event_list_stockItemsMouseClicked

    private void list_stockItemsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_list_stockItemsKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
            //System.out.println(KeyEvent.VK_DOWN);
//            invoiceView.txt_unitPrice.setBackground(Color.white);
//            invoiceView.txt_qtyAvb.setBackground(Color.white);
            list_stockItems.setSelectedIndex(++listIndex);
            if (listIndex > list_stockItems.getLastVisibleIndex()) {
                listIndex = 0;
                list_stockItems.setSelectedIndex(listIndex);
            }
            stockGetails();
        }
        if (evt.getKeyCode() == KeyEvent.VK_UP) {
//            invoiceView.txt_unitPrice.setBackground(Color.white);
//            invoiceView.txt_qtyAvb.setBackground(Color.white);
            list_stockItems.setSelectedIndex(--listIndex);
            if (listIndex == -1) {
                listIndex = list_stockItems.getLastVisibleIndex();
                list_stockItems.setSelectedIndex(listIndex);
            }
            stockGetails();
        }

        if (evt.getKeyCode() == KeyEvent.VK_RIGHT) {
            invoiceView.txt_qty.grabFocus();
            invoiceView.loopCount = 0;
            dispose();
        }
    }//GEN-LAST:event_list_stockItemsKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InvoiceStockItemsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InvoiceStockItemsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InvoiceStockItemsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InvoiceStockItemsView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InvoiceStockItemsView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> list_stockItems;
    // End of variables declaration//GEN-END:variables

    private void stockGetails() {
        invoiceView.txt_stockId.setText(list_stockItems.getSelectedValue().split(" - ")[0]);
        invoiceView.txt_unitPrice.setText(list_stockItems.getSelectedValue().split(" - ")[2]);
        invoiceView.txt_qtyAvb.setText(list_stockItems.getSelectedValue().split(" - ")[3]);
    }
}
